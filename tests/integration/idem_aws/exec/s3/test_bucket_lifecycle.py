import time

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get(hub, ctx, aws_s3_bucket_lifecycle):
    s3_bucket_lifecycle_get_name = "idem-test-exec-get-s3_bucket_lifecycle-" + str(
        int(time.time())
    )
    ret = await hub.exec.aws.s3.bucket_lifecycle.get(
        ctx,
        name=s3_bucket_lifecycle_get_name,
        resource_id=aws_s3_bucket_lifecycle["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert aws_s3_bucket_lifecycle["resource_id"] == resource.get("resource_id")
