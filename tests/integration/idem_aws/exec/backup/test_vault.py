import time
from typing import List

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_get(hub, ctx, aws_backup_vault):
    ret = await hub.exec.aws.backup.backup_vault.get(
        ctx,
        name=aws_backup_vault["name"],
        resource_id=aws_backup_vault["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert ret["ret"]["tags"] == {"Key": "resource", "Value": "backup_vault"}
    resource = ret["ret"]
    assert aws_backup_vault["name"] == resource.get("name")
    assert aws_backup_vault["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_get_invalid_resource_id(hub, ctx):
    vault_get_name = "idem-test-exec-get-vault-" + str(int(time.time()))
    ret = await hub.exec.aws.backup.backup_vault.get(
        ctx,
        name=vault_get_name,
        resource_id="fake-id",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get aws.backup.backup_vault '{vault_get_name}' result is empty" in str(
        ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_list(hub, ctx, aws_backup_vault):
    vault_get_name = "idem_backup_vault" + str(int(time.time()))
    ret = await hub.exec.aws.backup.backup_vault.list(ctx, name=vault_get_name)
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    resource = [
        item
        for item in ret["ret"]
        if item["resource_id"] == aws_backup_vault["resource_id"]
    ]
    assert resource
    assert resource[0]["resource_id"] == aws_backup_vault["resource_id"]
    assert resource[0]["tags"] == {"Key": "resource", "Value": "backup_vault"}
