import copy
import time
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idemstage" + str(int(time.time())),
    "tracing_enabled": False,
    "cache_cluster_enabled": False,
    "description": "Idem testing for AWS API Gateway Stage.",
    "tags": {"idem-resource-name-new": "new-val" + str(int(time.time()))},
}


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, aws_apigateway_deployment, cleanup):
    global PARAMETER
    rest_api_id = aws_apigateway_deployment.get("rest_api_id")
    deployment_id = aws_apigateway_deployment.get("resource_id")
    PARAMETER["rest_api_id"] = rest_api_id
    PARAMETER["deployment_id"] = deployment_id

    ctx["test"] = __test
    present_ret = await hub.states.aws.apigateway.stage.present(ctx, **PARAMETER)

    assert present_ret["result"], present_ret["comment"]
    resource = present_ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.apigateway.stage",
                name=PARAMETER["name"],
            )[0]
            in present_ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource.get("resource_id")
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.apigateway.stage",
                name=PARAMETER["name"],
            )[0]
            in present_ret["comment"]
        )
    assert not present_ret.get("old_state") and present_ret.get("new_state")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["rest_api_id"] == resource.get("rest_api_id")
    assert PARAMETER["deployment_id"] == resource.get("deployment_id")
    assert PARAMETER["cache_cluster_enabled"] == resource.get("cache_cluster_enabled")
    assert PARAMETER["tracing_enabled"] == resource.get("tracing_enabled")
    assert PARAMETER["tags"] == resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    global PARAMETER
    describe_ret = await hub.states.aws.apigateway.stage.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    assert "aws.apigateway.stage.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get("aws.apigateway.stage.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["tracing_enabled"] == described_resource_map.get("tracing_enabled")
    assert PARAMETER["cache_cluster_enabled"] == described_resource_map.get(
        "cache_cluster_enabled"
    )
    assert PARAMETER["deployment_id"] == described_resource_map.get("deployment_id")
    assert PARAMETER["name"] == described_resource_map.get("name")
    assert PARAMETER["rest_api_id"] == described_resource_map.get("rest_api_id")
    assert PARAMETER["resource_id"] == described_resource_map.get("resource_id")
    assert PARAMETER["description"] == described_resource_map.get("description")
    assert PARAMETER["tags"] == described_resource_map.get("tags")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update", depends=["present"])
async def test_update(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    new_parameter = copy.deepcopy(PARAMETER)
    new_parameter["tracing_enabled"] = True
    new_parameter["tags"] = {"NewKey": "NewVal"}
    ret = await hub.states.aws.apigateway.stage.present(
        ctx,
        **new_parameter,
    )
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.apigateway.stage",
                name=new_parameter["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type="aws.apigateway.stage",
                name=new_parameter["name"],
            )[0]
            in ret["comment"]
        )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret["new_state"]
    assert new_parameter["resource_id"] == resource.get("resource_id")
    assert new_parameter["rest_api_id"] == resource.get("rest_api_id")
    assert new_parameter["name"] == resource.get("name")
    assert new_parameter["deployment_id"] == resource.get("deployment_id")
    assert new_parameter["cache_cluster_enabled"] == resource.get(
        "cache_cluster_enabled"
    )
    assert new_parameter["description"] == resource.get("description")
    assert new_parameter["tracing_enabled"] == resource.get("tracing_enabled")
    assert new_parameter["tags"] == resource.get("tags")
    PARAMETER["tracing_enabled"] = resource.get("tracing_enabled")
    PARAMETER["tags"] = resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="remove_tags", depends=["update"])
async def test_removing_tags(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    new_parameter = copy.deepcopy(PARAMETER)
    new_parameter["tags"] = {}
    ret = await hub.states.aws.apigateway.stage.present(
        ctx,
        **new_parameter,
    )
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_tags_comment(
                tags_to_remove=PARAMETER["tags"], tags_to_add=new_parameter["tags"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_tags_comment(
                tags_to_remove=PARAMETER["tags"], tags_to_add=new_parameter["tags"]
            )[0]
            in ret["comment"]
        )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret["new_state"]
    assert new_parameter["resource_id"] == resource.get("resource_id")
    assert new_parameter["rest_api_id"] == resource.get("rest_api_id")
    assert new_parameter["name"] == resource.get("name")
    assert new_parameter["deployment_id"] == resource.get("deployment_id")
    assert new_parameter["cache_cluster_enabled"] == resource.get(
        "cache_cluster_enabled"
    )
    assert new_parameter["description"] == resource.get("description")
    assert new_parameter["tracing_enabled"] == resource.get("tracing_enabled")
    assert resource.get("tags") is None
    if not __test:
        PARAMETER["tags"] = resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="exec-get", depends=["remove_tags"])
async def test_exec_get(hub, ctx):
    global PARAMETER
    ret = await hub.exec.aws.apigateway.stage.get(
        ctx,
        resource_id=PARAMETER["resource_id"],
    )

    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["rest_api_id"] == resource.get("rest_api_id")
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["deployment_id"] == resource.get("deployment_id")
    assert PARAMETER["cache_cluster_enabled"] == resource.get("cache_cluster_enabled")
    assert PARAMETER["description"] == resource.get("description")
    assert PARAMETER["tracing_enabled"] == resource.get("tracing_enabled")
    assert PARAMETER["tags"] == resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get-not-found", depends=["present"])
async def test_get_resource_id_does_not_exist(hub, ctx):
    resource_id = "restApiId-stageName"
    ret = await hub.exec.aws.apigateway.stage.get(
        ctx,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get '{resource_id}' result is empty" and f"NotFoundException" in str(
        ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="absent", depends=["exec-get-not-found"])
async def test_absent_stage(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    ret = await hub.states.aws.apigateway.stage.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["rest_api_id"] == resource.get("rest_api_id")
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert PARAMETER["deployment_id"] == resource.get("deployment_id")
    assert PARAMETER["cache_cluster_enabled"] == resource.get("cache_cluster_enabled")
    assert PARAMETER["description"] == resource.get("description")
    assert PARAMETER["tracing_enabled"] == resource.get("tracing_enabled")
    assert PARAMETER["tags"] == resource.get("tags")

    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.apigateway.stage",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.apigateway.stage",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    ret = await hub.states.aws.apigateway.stage.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"]
    assert ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigateway.stage",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="absent_with_none_resource_id", depends=["already_absent"])
async def test_stage_absent_with_none_resource_id(hub, ctx):
    global PARAMETER
    # Delete Stage with resource_id as None. Result in no-op.
    ret = await hub.states.aws.apigateway.stage.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=None,
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigateway.stage",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )


@pytest.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if not hub.tool.utils.is_running_localstack(ctx):
        if "resource_id" in PARAMETER:
            ret = await hub.states.aws.apigateway.stage.absent(
                ctx,
                name=PARAMETER["name"],
                resource_id=PARAMETER["resource_id"],
            )
            assert ret["result"], ret["comment"]
