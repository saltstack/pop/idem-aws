import copy
import time
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(
    False,
    "Localstack is unable to create EKS resources, so test against a real AWS endpoint",
)
@pytest.mark.asyncio
async def test_eks_nodegroup(
    hub,
    ctx,
    aws_iam_worker_role_assignment,
    aws_ec2_subnet_for_eks_worker,
    aws_eks_cluster,
):
    cluster_id = aws_eks_cluster.get("resource_id")
    await is_cluster_active(hub, ctx, cluster_id)

    # node group creation
    nodegroup_temp_name = "idem-test-nodegroup-" + str(uuid.uuid4())
    worker_arn = aws_iam_worker_role_assignment["arn"]
    version = "1.27"
    release_version = "1.27.4-20230825"
    capacity_type = "ON_DEMAND"
    ami_type = "AL2_x86_64"
    instance_types = ["t3.micro"]
    disk_size = 20
    scaling_config = {"desiredSize": 2, "maxSize": 2, "minSize": 2}
    update_config = {"maxUnavailable": 1}
    tags = {"Name": nodegroup_temp_name}

    # create nodegroup with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    ret = await hub.states.aws.eks.nodegroup.present(
        test_ctx,
        name=nodegroup_temp_name,
        cluster_name=cluster_id,
        version=version,
        release_version=release_version,
        capacity_type=capacity_type,
        ami_type=ami_type,
        node_role=worker_arn,
        instance_types=instance_types,
        subnets=[aws_ec2_subnet_for_eks_worker.get("resource_id")],
        disk_size=disk_size,
        scaling_config=scaling_config,
        update_config=update_config,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type="aws.eks.nodegroup", name=nodegroup_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_nodegroup(
        resource,
        cluster_id,
        nodegroup_temp_name,
        version,
        release_version,
        capacity_type,
        ami_type,
        worker_arn,
        instance_types,
        disk_size,
        scaling_config,
        update_config,
        tags,
    )

    # create nodegroup in real
    ret = await hub.states.aws.eks.nodegroup.present(
        ctx,
        name=nodegroup_temp_name,
        cluster_name=cluster_id,
        version=version,
        release_version=release_version,
        capacity_type=capacity_type,
        ami_type=ami_type,
        node_role=worker_arn,
        instance_types=instance_types,
        subnets=[aws_ec2_subnet_for_eks_worker.get("resource_id")],
        disk_size=disk_size,
        scaling_config=scaling_config,
        update_config=update_config,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type="aws.eks.nodegroup", name=nodegroup_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_nodegroup(
        resource,
        cluster_id,
        nodegroup_temp_name,
        version,
        release_version,
        capacity_type,
        ami_type,
        worker_arn,
        instance_types,
        disk_size,
        scaling_config,
        update_config,
        tags,
    )
    resource_id = resource.get("resource_id")

    # Describe node group
    describe_ret = await hub.states.aws.eks.nodegroup.describe(ctx)
    assert f"{cluster_id}-{resource_id}" in describe_ret

    # Verify that describe output format is correct
    assert "aws.eks.nodegroup.present" in describe_ret.get(
        f"{cluster_id}-{resource_id}"
    )
    described_resource = describe_ret.get(f"{cluster_id}-{resource_id}").get(
        "aws.eks.nodegroup.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert_nodegroup(
        described_resource_map,
        cluster_id,
        f"{cluster_id}-{nodegroup_temp_name}",
        version,
        release_version,
        capacity_type,
        ami_type,
        worker_arn,
        instance_types,
        disk_size,
        scaling_config,
        update_config,
        tags,
    )

    # update with test flag
    new_scaling_config = {"desiredSize": 3, "maxSize": 3, "minSize": 3}
    new_update_config = {"maxUnavailable": 2}
    tags = {
        "Name": nodegroup_temp_name,
        f"idem-test-nodegroup-key-{str(uuid.uuid4())}": f"idem-test-nodegroup-value-{str(uuid.uuid4())}",
    }
    labels = {"idem-test-lable-key": "idem-test-label-value"}

    # update with test flag
    ret = await hub.states.aws.eks.nodegroup.present(
        test_ctx,
        name=nodegroup_temp_name,
        cluster_name=cluster_id,
        resource_id=resource_id,
        version=version,
        release_version=release_version,
        node_role=worker_arn,
        subnets=[aws_ec2_subnet_for_eks_worker.get("resource_id")],
        scaling_config=new_scaling_config,
        update_config=new_update_config,
        labels=labels,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"Would update aws.eks.nodegroup '{nodegroup_temp_name}'" in ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_nodegroup(
        resource,
        cluster_id,
        nodegroup_temp_name,
        version,
        release_version,
        capacity_type,
        ami_type,
        worker_arn,
        instance_types,
        disk_size,
        new_scaling_config,
        new_update_config,
        tags,
        labels,
    )

    # update nodegroup in real
    ret = await hub.states.aws.eks.nodegroup.present(
        ctx,
        name=nodegroup_temp_name,
        cluster_name=cluster_id,
        resource_id=resource_id,
        version=version,
        release_version=release_version,
        node_role=worker_arn,
        subnets=[aws_ec2_subnet_for_eks_worker.get("resource_id")],
        scaling_config=new_scaling_config,
        update_config=new_update_config,
        labels=labels,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_nodegroup(
        resource,
        cluster_id,
        nodegroup_temp_name,
        version,
        release_version,
        capacity_type,
        ami_type,
        worker_arn,
        instance_types,
        disk_size,
        new_scaling_config,
        new_update_config,
        tags,
        labels,
    )

    # Delete nodegroup with test flag
    ret = await hub.states.aws.eks.nodegroup.absent(
        test_ctx,
        name=nodegroup_temp_name,
        cluster_name=cluster_id,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.eks.nodegroup", name=nodegroup_temp_name
        )[0]
        in ret["comment"]
    )

    # Delete nodegroup
    ret = await hub.states.aws.eks.nodegroup.absent(
        ctx, name=nodegroup_temp_name, cluster_name=cluster_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.eks.nodegroup", name=nodegroup_temp_name
        )[0]
        in ret["comment"]
    )

    # Deleting nodegroup again should be a no-op
    ret = await hub.states.aws.eks.nodegroup.absent(
        ctx, name=nodegroup_temp_name, cluster_name=cluster_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.eks.nodegroup", name=nodegroup_temp_name
        )[0]
        in ret["comment"]
    )


async def is_cluster_active(hub, ctx, resource_id):
    for i in range(20):
        resource = await hub.exec.boto3.client.eks.describe_cluster(
            ctx, name=resource_id
        )
        if resource["result"] and resource["ret"]:
            cluster = resource["ret"]["cluster"]
            status = cluster["status"]
            if status != "ACTIVE":
                time.sleep(180)
            else:
                break


def assert_nodegroup(
    resource,
    cluster_id,
    nodegroup_temp_name,
    version,
    release_version,
    capacity_type,
    ami_type,
    worker_arn,
    instance_types,
    disk_size,
    scaling_config,
    update_config,
    tags,
    labels=None,
    taints=None,
):
    assert cluster_id == resource.get("cluster_name")
    assert nodegroup_temp_name == resource.get("name")
    assert version == resource.get("version")
    assert release_version == resource.get("release_version")
    assert capacity_type == resource.get("capacity_type")
    assert worker_arn == resource.get("node_role")
    assert ami_type == resource.get("ami_type")
    assert instance_types == resource.get("instance_types")
    assert disk_size == resource.get("disk_size")
    assert scaling_config == resource.get("scaling_config")
    assert update_config == resource.get("update_config")
    assert tags == resource.get("tags")
    assert labels == resource.get("labels")
    assert taints == resource.get("taints")


@pytest.mark.asyncio
async def test_eks_nodegroup_absent_with_none_resource_id(hub, ctx):
    nodegroup_temp_name = "idem-test-nodegroup-" + str(uuid.uuid4())
    # Delete eks node group with resource_id as None. Result in no-op.
    ret = await hub.states.aws.eks.nodegroup.absent(
        ctx, name=nodegroup_temp_name, cluster_name=None, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.eks.nodegroup", name=nodegroup_temp_name
        )[0]
        in ret["comment"]
    )
