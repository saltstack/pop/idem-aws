import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_flow_log(hub, ctx, aws_ec2_vpc, aws_s3_bucket):
    # create flow_log
    name = "idem-test-flow-log-" + str(uuid.uuid4())
    tags = [
        {"Key": "Name", "Value": name},
    ]
    bucket_name = aws_s3_bucket["name"]

    # Create flow log with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    log_format = (
        "${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport} ${dstport} ${protocol} "
        "${packets} ${bytes} ${start} ${end} ${action} ${log-status}"
    )
    vpc_id = aws_ec2_vpc.get("VpcId")
    ret = await hub.states.aws.ec2.flow_log.present(
        ctx=test_ctx,
        name=name,
        resource_type="VPC",
        traffic_type="ALL",
        log_destination_type="s3",
        iam_role=None,
        log_group_name=None,
        log_destination="arn:aws:s3:::" + bucket_name,
        log_format=log_format,
        max_aggregation_interval=600,
        resource_ids=[vpc_id],
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert any("Would create aws.ec2.flow_log" in c for c in ret["comment"])
    assert not ret.get("old_state") and ret.get("new_state")
    await assert_flow_log(hub, ret, name, tags, bucket_name, log_format, vpc_id)

    # Create flow log in real
    ret = await hub.states.aws.ec2.flow_log.present(
        ctx=ctx,
        name=name,
        resource_type="VPC",
        traffic_type="ALL",
        log_destination_type="s3",
        log_destination="arn:aws:s3:::" + bucket_name,
        log_format="${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport} ${dstport} ${protocol} "
        "${packets} ${bytes} ${start} ${end} ${action} ${log-status}",
        max_aggregation_interval=600,
        resource_ids=[aws_ec2_vpc.get("VpcId")],
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    await assert_flow_log(hub, ret, name, tags, bucket_name, log_format, vpc_id)

    assert bucket_name in ret["new_state"]["log_destination"]
    created_flow_log_id = ret["new_state"]["resource_id"]

    # verify that created flow log is present
    describe_ret = await hub.states.aws.ec2.flow_log.describe(ctx)
    assert created_flow_log_id in describe_ret
    resource = describe_ret[created_flow_log_id].get("aws.ec2.flow_log.present")
    described_resource_map = dict(ChainMap(*resource))
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        [aws_ec2_vpc.get("VpcId")], described_resource_map.get("resource_ids")
    )
    assert "VPC" == described_resource_map.get("resource_type")
    assert created_flow_log_id == described_resource_map.get("resource_id")
    assert "ALL" == described_resource_map.get("traffic_type")
    assert "s3" == described_resource_map.get("log_destination_type")
    assert "arn:aws:s3:::" + bucket_name == described_resource_map.get(
        "log_destination"
    )
    assert log_format == described_resource_map.get("log_format")
    assert 600 == described_resource_map.get("max_aggregation_interval")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        hub.tool.aws.tag_utils.convert_tag_dict_to_list(
            described_resource_map.get("tags")
        ),
        tags,
    )

    new_tags = [
        {"Key": "new-name", "Value": created_flow_log_id},
    ]
    # Update flow log with test flag
    ret = await hub.states.aws.ec2.flow_log.present(
        ctx=test_ctx,
        name=name,
        resource_id=created_flow_log_id,
        resource_type="VPC",
        traffic_type="ALL",
        log_destination_type="s3",
        log_destination="arn:aws:s3:::" + bucket_name,
        log_format="${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport} ${dstport} ${protocol} "
        "${packets} ${bytes} ${start} ${end} ${action} ${log-status}",
        max_aggregation_interval=600,
        resource_ids=[aws_ec2_vpc.get("VpcId")],
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert "already exists. Updating tags" in ret["comment"][0]
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        hub.tool.aws.tag_utils.convert_tag_dict_to_list(ret["new_state"].get("tags")),
        new_tags,
    )
    resource = ret.get("old_state")
    assert "VPC" == resource.get("resource_type")
    assert "ALL" == resource.get("traffic_type")
    assert "s3" == resource.get("log_destination_type")
    assert "arn:aws:s3:::" + bucket_name == resource.get("log_destination")
    assert log_format == resource.get("log_format")
    assert 600 == resource.get("max_aggregation_interval")

    # update tags in real
    ret = await hub.states.aws.ec2.flow_log.present(
        ctx,
        name=created_flow_log_id,
        resource_id=created_flow_log_id,
        resource_type="VPC",
        traffic_type="ALL",
        log_destination_type="s3",
        log_destination="arn:aws:s3:::" + bucket_name,
        log_format="${version} ${account-id} ${interface-id} ${srcaddr} ${dstaddr} ${srcport} ${dstport} ${protocol} "
        "${packets} ${bytes} ${start} ${end} ${action} ${log-status}",
        max_aggregation_interval=600,
        resource_ids=[aws_ec2_vpc.get("VpcId")],
        tags=new_tags,
    )
    assert ret["result"]
    assert "new_state" in ret
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        hub.tool.aws.tag_utils.convert_tag_dict_to_list(ret["new_state"].get("tags")),
        new_tags,
    )

    # Delete flow log with test flag
    ret = await hub.states.aws.ec2.flow_log.absent(
        test_ctx, name=name, resource_id=created_flow_log_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.ec2.flow_log", name=name
        )[0]
        in ret["comment"]
    )

    # Delete flow log in real
    ret = await hub.states.aws.ec2.flow_log.absent(
        ctx, name=name, resource_id=created_flow_log_id
    )
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.ec2.flow_log", name=name
        )[0]
        in ret["comment"]
    )
    assert ret["old_state"] and not ret["new_state"]

    # Deleting the same instance again should state the same in comment
    ret = await hub.states.aws.ec2.flow_log.absent(
        ctx, name=name, resource_id=created_flow_log_id
    )
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.ec2.flow_log", name=name
        )[0]
        in ret["comment"]
    )
    assert not ret["old_state"] and not ret["new_state"]


async def assert_flow_log(
    hub,
    ret,
    name,
    tags,
    bucket_name,
    log_format,
    vpc_id,
):
    assert ret["result"], ret["comment"]
    assert ret["name"] == name
    resource = ret.get("new_state")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        tags, hub.tool.aws.tag_utils.convert_tag_dict_to_list(resource["tags"])
    )
    assert "VPC" == resource.get("resource_type")
    assert "ALL" == resource.get("traffic_type")
    assert "s3" == resource.get("log_destination_type")
    assert "arn:aws:s3:::" + bucket_name == resource.get("log_destination")
    assert log_format == resource.get("log_format")
    assert 600 == resource.get("max_aggregation_interval")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        [vpc_id], resource.get("resource_ids")
    )
