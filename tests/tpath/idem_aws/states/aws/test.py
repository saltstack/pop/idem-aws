import copy

__contracts__ = ["resource", "allow_sync_sls_name_and_name_tag"]


def present(hub, ctx, name: str, resource_id: str = None, tags=None, **kwargs):
    kwargs["resource_id"] = resource_id
    kwargs["name"] = name
    kwargs["tags"] = tags
    result = dict(comment=[], old_state=None, new_state=kwargs, name=name, result=True)
    return result


def absent(hub, ctx, name: str, resource_id: str = None, **kwargs):
    kwargs["resource_id"] = resource_id
    kwargs["name"] = name
    result = dict(comment=[], old_state=None, new_state=kwargs, name=name, result=True)
    return result


async def describe(hub, ctx):
    result = {}

    for i in range(10):
        f"aws-test-resource-{i}"
        result[i] = {"aws.test.present": {"resource_id": i, "name": i}}

    return result


async def acct(hub, ctx, name: str):
    profile = copy.deepcopy(ctx.acct)

    # Use the profile
    ret = await hub.exec.aws.ec2.vpc.get(ctx, name=name)

    # Remove "config" as it is not easily used in comparisons
    profile.pop("config")
    return dict(
        comment=ret.comment,
        old_state=None,
        new_state=profile,
        name=name,
        result=ret.result,
    )
